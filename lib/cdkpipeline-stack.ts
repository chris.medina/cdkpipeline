import * as cdk from '@aws-cdk/core';
import * as codepipeline from '@aws-cdk/aws-codepipeline';
import * as codepipeline_actions from '@aws-cdk/aws-codepipeline-actions';
import * as codebuild from '@aws-cdk/aws-codebuild';
import * as s3 from '@aws-cdk/aws-s3';
import * as iam from '@aws-cdk/aws-iam';
import { PolicyStatement, Role, ServicePrincipal } from '@aws-cdk/aws-iam';

export class CdkpipelineStack extends cdk.Stack {
  codePipeline: codepipeline.Pipeline;
  sourceOutput: codepipeline.Artifact;
  outputWebsite: codepipeline.Artifact;
  project: codebuild.PipelineProject;
  cacheBucket: s3.IBucket;
  webBucket: s3.IBucket;
  deployRole: iam.Role;

  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);
    this.createArtifacts();
    this.createCacheBucket();
    this.createWebBucket();
    this.createCodeBuildProject();
    this.createPipeline();
  }

  createArtifacts(): void {
    // Not sure if this is needed
    this.sourceOutput = new codepipeline.Artifact();
    this.outputWebsite = new codepipeline.Artifact();
  }

  createCacheBucket(): void {
    this.cacheBucket = new s3.Bucket(this, 'cacheBucket');
  }

  createWebBucket(): void {
    this.webBucket = new s3.Bucket(this, 'hearthstone_badmage_site', {
      bucketName: 'hearthstone.badmage.gg',
      publicReadAccess: true,
      removalPolicy: cdk.RemovalPolicy.DESTROY,
      websiteIndexDocument: 'index.html',
      websiteErrorDocument: '404.html',
    });
  }

  createCodeBuildProject(): void {
    this.project = new codebuild.PipelineProject(this, 'hearthstone_badmage', {
      cache: codebuild.Cache.bucket(this.cacheBucket),
    });
  }

  createPipeline(): void {
    this.codePipeline = new codepipeline.Pipeline(
      this,
      'hearthstoneBadMageSite',
      {
        pipelineName: 'Hearthstone',
        crossAccountKeys: false,
        stages: [
          {
            stageName: 'Source',
            actions: [
              new codepipeline_actions.GitHubSourceAction({
                actionName: 'Gitub_Source',
                owner: 'TheMedina',
                repo: 'hearthstone.badmage.gg',
                oauthToken: cdk.SecretValue.secretsManager(
                  'hearthstone_badmage_github'
                ),
                output: this.sourceOutput, // Not sure if this needed
                branch: 'main',
                trigger: codepipeline_actions.GitHubTrigger.WEBHOOK,
              }),
            ],
          },
          {
            stageName: 'Build',
            actions: [
              new codepipeline_actions.CodeBuildAction({
                actionName: 'CodeBuild',
                project: this.project,
                input: this.sourceOutput, // Not sure if this is needed
                outputs: [this.outputWebsite],
                executeBatchBuild: false,
              }),
            ],
          },
          {
            stageName: 'Deploy',
            actions: [
              new codepipeline_actions.S3DeployAction({
                actionName: 'Website',
                input: this.outputWebsite,
                bucket: this.webBucket,
              }),
            ],
          },
        ],
      }
    );
  }
}
